(function () {

    window.App = {
        Models: {},
        Views: {},
        Collections: {}
    };


    window.template = function (id) {
        return _.template($('#' + id).html());
    };

    $(document).ready(function () {
        App.Models.Person = Backbone.Model.extend({
            defaults: {
                name: 'Boris',
                age: 28,
                job: 'web developer'
            },
            validate: function (attrs, options) {
                console.log(attrs);
                if (attrs.age <= 0) {
                    return 'Error: Negative age!';
                }
                if (!attrs.name) {
                    return 'Error: Input name of person'
                }
            },
            walk: function () {
                return this.get('name') + ' is walking!'
            }
        });
        App.Views.Person = Backbone.View.extend({
            initialize: function () {
                this.render();
            },
            tagName: 'li',

            //template: _.template($('#person-id').html()),
            template: template('person'),
            //template: _.template('<strong><%= name %>(<%= age %>)</strong> - <%= job %>'),
            render: function () {
                this.$el.html(this.template(this.model.toJSON()));
                return this;
            }
        });

        App.Views.People = Backbone.View.extend({
            tagName: 'ul',
            initialize: function () {
                console.log(this.collection);
            },
            render: function () {
                this.collection.each(function (person) {
                    var personView = new App.Views.Person({model: person});
                    this.$el.append(personView.render().el)
                }, this);

                return this;
            }
        });
        App.Collections.People = Backbone.Collection.extend({
            model: App.Models.Person
        });


        var peopleCollection = new App.Collections.People([
            {name: 'Vasya'},
            {name: 'Petya'},
            {name: 'Misha'}
        ]);

        var peopleView = new App.Views.People({collection: peopleCollection});


        peopleView.render();
        $(document.body).append(peopleView.el);
    });

})();


//people.add(person);
//people.add(person2);
//one.on("invalid", function(model, error) {
//    alert(model.get("title") + " " + error);
//});
//console.log(person.get('age'));
//person.save({age: -234});
//console.log(person.get('age'));
//
//person.on('invalid', function (model, error) {console.log(error);});

//
//(config) {
//    this.name = config.name;
//    this.age = config.age;
//    this.job = config.job;
//};
//
//Person.prototype.walk = function () {
//    return this.name + ' is walking!'
//}
//
//
//var nick = new Person({name: "Nick", age: 24, job: "loftblog"});
//
//var Chapter = Backbone.Model.extend({
//    validate: function(attrs, options) {
//        console.log(attrs, options);
//        if (attrs.end < attrs.start) {
//            return "can't end before it starts";
//        }
//    }
//});
//
//var one = new Chapter({
//    title : "Chapter One: The Beginning"
//});
//
//one.on("invalid", function(model, error) {
//    console.log(model.get("title") + " " + error);
//});
//
//one.save({
//    start: 15,
//    end:   10
//});

//one.on('error', function (model, error) {console.log(error);});
//
//if (!one.isValid()) {
//    console.log(one.get("title") + " " + one.validationError);
//}