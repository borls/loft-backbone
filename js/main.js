(function () {

    // пространство имен
    window.App = {
        Models: {},
        Views: {},
        Collections: {},
        Router: {}
    };

    // шаблон
    window.template = function (id) {
        return _.template($('#' + id).html());
    };

    $(document).ready(function () {

        var vent = _.extend({}, Backbone.Events);
        console.log(vent);

        App.Views.SpecialTask = Backbone.View.extend({
            initialize: function () {
                vent.on('specialTasks:show', this.show, this)
            },
            show: function (id) {
                console.log('The task', id);
                var specialTask = this.collection.get('id');
                var specialTaskView = new App.Views.SpecialTask({model: specialTask});
                $('body').append(specialTaskView.render().el);
            }
        });
        App.Router = Backbone.Router.extend({
            routes: {
                '': 'index',
                'specialTasks/:id': 'showspecialTasks',
                'page/:id/*symbols': 'page',
                'search/:query': 'search',
                '*some': 'default'
            },
            index: function () {
                console.log('Hi there! Im index router! :P');
            },
            page: function (id, symbols) {
                console.log('router page', id, symbols)
            },
            search: function (query) {
                console.log(query)
            },
            showspecialTasks: function (id) {
                vent.trigger('specialTasks:show', id);
            },
            default: function (some) {
                console.error('no such router!', some);
            }
        });
        new App.Views.SpecialTask({collection: App.Collections.Task});
        new App.Router();
        Backbone.history.start();

        App.Models.Task = Backbone.Model.extend({
            validate: function (attrs, options) {
                if (!$.trim(attrs.title)) {
                    return 'Name of task must be valid!';
                }
            }
        });

        App.Views.Task = Backbone.View.extend({
            initialize: function () {
                this.model.on('change', this.render, this);
                this.model.on('destroy', this.remove, this);
            },
            tagName: 'li',
            template: template('taskTmp'),
            render: function () {
                var tmp = this.template(this.model.toJSON());
                this.$el.html(tmp);
                return this;
            },
            remove: function () {
                this.$el.remove();
            },
            events: {
                'click .btn-edit': 'editTask',
                'click .btn-delete': 'deleteTask'
            },
            editTask: function () {
                console.log('edit task:', this.model.get('title'));
                var title = prompt('Задача', this.model.get('title'));
                this.model.save('title', title);
            },
            deleteTask: function () {
                this.model.destroy();
            }
        });

        App.Collections.Task = Backbone.Collection.extend({
            model: App.Models.Task
        });

        App.Views.Tasks = Backbone.View.extend({
            tagName: 'ul',
            initialize: function () {
                this.collection.on('add', this.addOne, this);
            },
            render: function () {
                this.collection.each(this.addOne, this);
                return this;
            },
            addOne: function (task) {
                var taskView = new App.Views.Task({model: task});
                this.$el.append(taskView.render().el);
            }
        });

        App.Views.AddTask = Backbone.View.extend({
            el: '#add-task',
            events: {
                'submit': 'submit'
            },
            initialize: function () {
                console.log(this.el.innerHTML)
            },
            submit: function (e) {
                e.preventDefault();

                var taskTitle = $(e.currentTarget).find('input[type=text]').val();

                var task = new App.Models.Task({title: taskTitle});
                this.collection.add(task);
                console.log(this.collection.toJSON());
            }
        });
        var tasks = new App.Collections.Task([
            {
                title: 'coding site',
                priority: 3
            }, {
                title: 'learn backbone',
                priority: 2
            }, {
                title: 'meet with Ornella',
                priority: 1
            }
        ]);

        var tasksView = new App.Views.Tasks({collection: tasks});

        $('.tasks').html(tasksView.render().el);

        var addTaskView = new App.Views.AddTask({collection: tasks});

    });

})();